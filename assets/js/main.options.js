$(document).ready(function(){
    function menu(){
        var iconMenu  = $('.menu');
        iconMenu.click(function(e){
            e.stopPropagation();
            var nav_xs   = $('.navigation');
            nav_xs.toggleClass('open');
        });
        $('.navigation').removeClass('open');
    }

    //Menu
    menu();

    var imgScroll = function(){
        var $limitArea = $('.limitArea'),
            $window = $(window),
            bottomLine =  $limitArea.offset().top + $limitArea.height(),
            sdegree = 0.
       
        $window.scroll(function() {
            var rotateMove =  $window.scrollTop() - $limitArea.offset().top;
                result = rotateMove / 35;
            if($window.scrollTop() > $limitArea.offset().top && $window.scrollTop() <= bottomLine){
                var srotate = "rotate(" + result + "deg)";
            }
            $(".img-rotate").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});
        }); 
    }

    //EQUAL HEIGHT
    function equalHeight(group) {
        tallest = 0;
        $(window).on("load resize", function(){
            if ($(window).width() > 979 ) {
                group.each(function() {
                    thisHeight = $(this).height();
                    if(thisHeight > tallest) {
                        tallest = thisHeight;
                    }
                });
                group.height(tallest);
            }
        });
    }
    equalHeight($(".eqHeight"));

    //SMOOTH SCROLLING TARGET
    $('a[href*="#"]:not([href="#"]).smoothSCroll').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });

   //PRICING TABS
   $('.paidtimeNav li').click(function(){
        var $nav = $('.paidtimeNav li'),
            $parent = $(this).closest('.pricing-box');
            selected_tab = $(this).find('a').attr('href');
        $(this).closest('.pricing-box').find('li').removeClass();
        $(this).addClass('active');
        $(this).closest('.pricing-box').find(".paidtime-content").hide();
        $(this).closest('.pricing-box').find('.'+ selected_tab).fadeIn();
        return false;
   });


   $("input[type=radio]").click(function () {
        if ($("input[type=radio]:checked").val() == '1') {
            $(".inputToggle").slideDown();
        } else {
            $(".inputToggle").slideUp();
        }
    });

    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });
    
   //CAROUSEL
    $(".carousleSlide").slick({
        dots: false,
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive :[
        {
                breakpoint: 797,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
      });


    $(".blogSlide").slick({
        dots: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive :[
        {
                breakpoint: 797,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
      });

});
